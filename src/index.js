const Web3 = require('web3');

MORALIS_API = "https://speedy-nodes-nyc.moralis.io/fd0d8f36c9c1f2a508254528/polygon/mumbai";
WALLET_ADDRESS = "0x2c844b941C67b24C799Fd43bAd117cDF7F23ef81";
WALLET_PRIVATE_KEY = "";
DERC20_ABI = require('../abis/erc20');
DERC20_CONTRACT_ADDRESS = "0xfe4F5145f6e09952a5ba9e956ED0C25e3Fa4c7F1";

const web3 = new Web3(MORALIS_API);

async function init() {
  const maticBalance = await web3.eth.getBalance(WALLET_ADDRESS)/1000000000000000000;
  // token = new web3.eth.Contract(DERC20_ABI)
  const tokenContract = new web3.eth.Contract(DERC20_ABI, DERC20_CONTRACT_ADDRESS);
  // const tokenContract = new web3.eth.Contract(DERC20_CONTRACT_ADDRESS);
  const tokenBalance = await tokenContract.methods.balanceOf(WALLET_ADDRESS).call()/1000000000000000000;
  console.log("Showing balance for testnet wallet");
  console.log("MATIC balance: ", maticBalance);  
  console.log("DERC20 balance: ", tokenBalance);  
}

function submitHumanInfo() {
  return true;
}

function readHumanInfo() {
  return true;
}

function sendGLODistribution() {
  return true;
}

function humanInfo() {
  let johnDoe = {
    "user123": {
      "name": "John Doe Of Denham",
      "dob": "10-10-1980",
      "nationality": "NL",
      "email": "john.doe@denham.com",
      "tel": "123",
      "cob": "abc",
      "pob": "def",
      "country":"ghi",
      "city": "jkl",
      "street": "mno",
      "nin": "pqr",
      "idn": "stu",
      "kyc_date": new Date().toISOString().split('T')[0],
      "wallet": "0xD6B2fed043153cC9A4698773b8721237626ecF29",
    }
  };
  return johnDoe;
}


init();


